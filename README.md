# LazyPHP
A PHP Framework to begginers

## Description
LazyPHP is a small framework extremly simple and didatic to build web applications on MVC software archteture using PHP and MySQL

## Objective
The LazyPHP is a simple framework tha was born with didatic objectives, mainly teach beginners developers PHP concepts like: Object Oriented Programming, PDO, ORM, MVC software archteture, besides Project Patterns like Active Record, Singleton, Factory, etc., what eventually will be seen on source of project

## Usage
- First of all, clone the repository:
    ```
    git clone https://github.com/LeonardsonCC/lazyphp.git
    ```

    or download a release in [here](https://github.com/LeonardsonCC/lazyphp/releases).

- Configure your apache server, like ahead:
    - Enable **mod_rewrite**;
    - Use **PHP 5.3+** (PHP 7 not fully working);
    - Use **MySQL 5.5+**;
    - If using Linux and Lamp, make sure the **apache2.conf** have the lines: 
    ``` 
    <Directory /var/www/html>
        Option Indexes FollowSymLinks MultiViews
        AllowOverride All
        Require all granted
    </Directory>
    ```
- Configure LazyPHP
    - In the root folder of your project, you'll find a file called **config.php**, that's the file have the majority of configurations to the project.
    - Start configuring your DataBase settings, on lines between 9 and 16. 

- Starting the CRUD creation
    - On your browser, open the URL of your project, you'll see a simple home page.
    - Change the URL to **http://*<your_project_url>*/install** it'll show up an installation page.
    - Use that to configure and create the basic CRUD for all your tables on the database selected in **config.php**.