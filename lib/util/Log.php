<?php

/**
 * Class to log anything
 * 
 * @author Leonardson Cabral de Carvalho <leonardson.carvalho@gmail.com>
 */

class Log {

	// LOG LEVELS
	public static $ERROR = 1;
	public static $WARNING = 2;
	public static $INFO = 3;
	public static $DB = 4;
	public static $DEBUG = 5;
	
	// LOG FILE AND SIZE
	public static $FILENAME = 'lazyphp.log';
	public static $PATH_LOG = '';

	// Value in KB. 2048 = 2M
	public static $MAX_FILEZISE = 10240;

	// Will get the log file
	public static function getLogFile () {
		$log_file = self::$PATH_LOG === '' ?  __DIR__ . '/../../logs/' . self::$FILENAME : self::$PATH_LOG . '/' . self::$FILENAME;
		return $log_file;
	}

	public static function error ($message) {
		$b = debug_backtrace();
		$debug = array();
		$debug['class'] = $b[1]['class'];
		$debug['function'] = $b[1]['function'];
		
		$file = self::getLogFile();

		$file = fopen(self::getLogFile(), 'a');
		fwrite($file, self::createLine($message, $debug, self::$ERROR));
		fclose($file);

		return TRUE;
	}

	public static function warning ($message) {
		$b = debug_backtrace();
		$debug = array();
		$debug['class'] = $b[1]['class'];
		$debug['function'] = $b[1]['function'];
		
		$file = self::getLogFile();

		$file = fopen(self::getLogFile(), 'a');
		fwrite($file, self::createLine($message, $debug, self::$WARNING));
		fclose($file);

		return TRUE;
	}

	public static function info ($message) {
		$b = debug_backtrace();
		$debug = array();
		$debug['class'] = $b[1]['class'];
		$debug['function'] = $b[1]['function'];
		
		$file = self::getLogFile();

		$file = fopen(self::getLogFile(), 'a');
		fwrite($file, self::createLine($message, $debug, self::$INFO));
		fclose($file);

		return TRUE;
	}

	public static function database ($message) {
		$b = debug_backtrace();
		$debug = array();
		$debug['class'] = $b[1]['class'];
		$debug['function'] = $b[1]['function'];
		
		$file = self::getLogFile();

		$file = fopen(self::getLogFile(), 'a');
		fwrite($file, self::createLine($message, $debug, self::$DB));
		fclose($file);

		return TRUE;
	}

	public static function debug ($message) {
		$b = debug_backtrace();
		$debug = array();
		$debug['class'] = $b[1]['class'];
		$debug['function'] = $b[1]['function'];
		
		$file = self::getLogFile();

		$file = fopen(self::getLogFile(), 'a');
		fwrite($file, self::createLine($message, $debug, self::$DEBUG));
		fclose($file);

		return TRUE;
	}

	public static function createLine ($message, $debug, $level) {
		switch ($level) {
			case self::$ERROR: 		$level = 'ERROR'; break;
			case self::$WARNING: 	$level = 'WARNI'; break;
			case self::$INFO: 		$level = 'INFOR'; break;
			case self::$DB: 		$level = 'DATAB'; break;
			case self::$DEBUG: 		$level = 'DEBUG'; break;
			default:
				$level = 'UKNOW';
		}

		$line = date('d/m/Y H:i:s') . ' | ' . $level . ' | (' . $debug['class'] . '->' . $debug['function'] . ') => ' . $message . "\n";

		return $line;
	}
}