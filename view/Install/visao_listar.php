<div class="card card-info" id="ConfigVisaoListar_<?php echo $t->name; ?>">
    <div class="card-header">
        <button class="btn btn-sm btn-secondary mr-2" type="button" data-toggle="collapse" data-parent="#configs-acordion<?php echo $id; ?>" href="#collapseVisaoListar_<?php echo $t->name; ?>">
            <i class="fas fa-cog"></i>
        </button>
        Configurar Visão <strong>listar</strong>
    </div>
    <div id="collapseVisaoListar_<?php echo $t->name; ?>" class="card-collapse collapse">
        <div class="card-body">

            <div class="col-md-12">
                <h6>Colunas que deverão aparecer na listagem</h6>

                <table class="table table-striped table-bordered">
                    <thead class="thead-dark">
                        <tr>
                            <th>Exibir?</th>
                            <th>Atributo</th>
                            <th>Coluna vinculada</th>
                        </tr>
                    </thead>
                    <?php
                    $tbn = $t->name;
                    $tableSchema = $this->getTableSchema($t->name);
                    foreach ($tableSchema as $row) :
                        $priField = NULL;
                        if ($row->Key == 'PRI') {
                            $priField = $row->Field;
                        }
                        ?>
                        <tr>
                            <td>
                                <?php $checked = $priField != $row->Field ? 'checked' : ''; ?>
                                <input name="colunasAll_<?php echo $t->name; ?>[]" value="<?php echo $row->Field; ?>" type="checkbox" <?php echo $checked ?>>
                            </td>
                            <td><?php echo $row->Field; ?></td>
                            <td>
                                <?php
                                    $fk = FALSE;
                                    foreach ($dbschema as $dba) {
                                        if ($t->name == $dba->table) {

                                            $Ftableschema = $this->getTableSchema($dba->reftable);
                                            $strField = $Ftableschema[0]->Field;
                                            foreach ($Ftableschema as $f) {
                                                if ($f->Key != 'PRI') {
                                                    $strField = $f->Field;
                                                    break;
                                                }
                                            }

                                            if ($row->Field == $dba->fk) {
                                                echo '<select class="form-control" name="colunasTitleRef_' . $t->name . '_' . $row->Field . '">';
                                                foreach ($this->getTableSchema($dba->reftable) as $tbl) {
                                                    $selected = $strField == $tbl->Field ? 'selected' : '';
                                                    echo '<option value="' . $tbl->Field . '" ' . $selected . '>' . $dba->reftable . '.' . $tbl->Field . '</option>';
                                                }
                                                echo "</select>";
                                                $fk = true;
                                                break;
                                            }
                                        }
                                    }
                                    echo !$fk ? $row->Field : '';
                                    ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </table>

                <hr />
                <h6>Formulário de pesquisa</h6>
                <div class="form-group">
                    <label>Pesquisar por:</label>
                    <select name="pesquisa_<?php echo $t->name; ?>" class="form-control">
                        <?php
                        $selected = false;
                        foreach ($tableSchema as $row) :
                            if (!$selected && $row->Key != 'PRI') {
                                echo '<option value=' . $row->Field . ' selected>' . $row->Field . '</option>';
                                $selected = true;
                            } else {
                                echo '<option value=' . $row->Field . '>' . $row->Field . '</option>';
                            }
                        endforeach;
                        ?>
                    </select>
                </div>
            </div>

        </div>
    </div>
</div>